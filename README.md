Shared Library in Go
====================

This project is a example of **how to make a Shared Library in Go**, and **how to use a Shared Library in Go**

This is, actually, very simple. So, this project is divided in two parts:

*   The Library (in the `Library` directory)
*   The Application (in the `Application` directory)

Building the Library
--------------------

To build the _Library_ just open the folder in a _Terminal_ and run `make`. This will generate a `libfoo.h` (Header file) and a `libfoo.so` (The Shared Library)

You also can test it. There's a `test` folder with a `main.cc` (**C++** Program File) that import the `foo` library and test his functions. If you run `make test/exe`, this test will compile, linking the library, and run the test. To run it you should run the following command

```sh
# Assuming you are located in `Library` folder
LD_LIBRARY_PATH=$(LD_LIBRARY_PATH):../Library ./test/exe
```

Building the Application
------------------------

The _Application_ is just a program that import the **Shared Library** (the _Library_ folder's project) and call his functions. This is done by **CGO** (you can see more about in [the `go` command documentation](https://pkg.go.dev/cmd/go) and [the `cgo` command documentation](https://pkg.go.dev/cmd/cgo)), basically it _includes_ the generated Header File and _link_ the generated Shared Library (via `#cgo` pseudo directives)

To build the _Application_, again, just open the folder in a _Terminal_ and run `make`. But make sure you have builded _Library_ first, otherwise will not compile!
