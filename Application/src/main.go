package main

import (
    "unsafe"
)

/* Note: Before the `import "C"` statement, all these C Preprocessor
 * Directives are consumed
 */

//#cgo CFLAGS: -I../../Library/
//#cgo LDFLAGS: -L../../Library/ -lfoo
//#include "libfoo.h"
//#include <stdlib.h>
import "C"

func main() {
    // Every time we parse a Go String to a C String (`char*`)
    // we need to manually free space.
    name := C.CString("John Doe")
    defer C.free(unsafe.Pointer(name))

    C.SayHello(name)
    C.SayBye(name)
}
