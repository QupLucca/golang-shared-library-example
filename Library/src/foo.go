package main

import (
    "C"
    "fmt"
)

/* 
 * 
 */

//export SayHello
func SayHello(name *C.char) {
    pname := C.GoString(name)
    fmt.Printf("Hello, %s!\n", pname)
}

//export SayBye
func SayBye(name *C.char) {
    pname := C.GoString(name)
    fmt.Printf("Bye, %s!\n", pname)
}

func main() {}
